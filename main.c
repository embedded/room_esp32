#include <stdio.h>
#include <string.h>

#include "xtimer.h"
#include "memarray.h"
#include "senml/saul.h"
#include "periph/pm.h"

#include "coap.h"

#ifndef STARTUP_TIME
#define STARTUP_TIME (5)
#endif

#ifndef MEASUREMENT_INTERVAL
#define MEASUREMENT_INTERVAL (30)
#endif

#ifndef MAX_ERRORS
#define MAX_ERRORS (300/MEASUREMENT_INTERVAL)
#endif

/* Measurement interval and startup time in microseconds */
static const uint32_t measurement_interval = MEASUREMENT_INTERVAL * US_PER_SEC;
static const uint32_t startup_time = STARTUP_TIME * US_PER_SEC;

int main(void)
{
    coap_stats_t stats = {0};
    uint16_t saul_errors = 0;
    uint16_t send_errors = 0;

    coap_pkt_t pdu;
    sock_udp_ep_t remote;
    uint8_t buf[CONFIG_GCOAP_PDU_BUF_SIZE];
    char out[2 * CONFIG_GCOAP_PDU_BUF_SIZE];

    /* Initialise CoAP */
    int ret = coap_init(&remote, &stats);

    if (ret != 0) {
        printf("CoAP initialisation error: %d\n", ret);
        return 1;
    }

    /* Initialize CoAP packet */
    gcoap_req_init(&pdu, buf, CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_POST,
                   COAP_RESOURCE);
    coap_hdr_set_type(pdu.hdr, COAP_TYPE_CON);
    coap_opt_add_format(&pdu, COAP_FORMAT_CBOR);
    size_t hdr_len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);

    /* Start measurement loop */
    xtimer_ticks32_t last_wakeup = xtimer_now();

    /* Wait for network connection */
    xtimer_periodic_wakeup(&last_wakeup, startup_time);

    while (1) {
        size_t len = senml_saul_encode_cbor(pdu.payload, pdu.payload_len, saul_reg);
        if (len == 0) {
            printf("SenML/SAUL error\n");
            saul_errors++;
        }
        else {
            /* Log the CBOR data for debugging */
            fmt_bytes_hex(out, pdu.payload, len);
            out[2 * len] = '\0';
            printf("CBOR (%i B): %s\n", len, out);

            pdu.hdr->id++;
            if (coap_send(buf, hdr_len + len, &remote) != hdr_len + len) {
                print_str("Error sending measurement over CoAP\n");
                send_errors++;
            }
        }

        uint16_t errors = saul_errors + send_errors +
                          stats.errors + stats.timeouts;
        printf("Statistics:\n" \
              "  SenML/SAUL errors:      %3i\n" \
              "  CoAP send errors:       %3i\n"\
              "  CoAP success responses: %3i\n"\
              "  CoAP error responses:   %3i\n"\
              "  CoAP timeouts:          %3i\n"\
              "  Total errors:           %3i/%i\n",
               saul_errors, send_errors,
               stats.successes, stats.errors, stats.timeouts,
               errors, MAX_ERRORS);
        if (errors >= MAX_ERRORS) {
            printf("Max failures reached. Rebooting!\n");
            pm_reboot();
            return 2;
        }

        xtimer_periodic_wakeup(&last_wakeup, measurement_interval);
    }

    return 0;
}
