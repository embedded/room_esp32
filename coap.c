#include "coap.h"

coap_stats_t *coap_stats;

/*
 * Initialize CoAP networking
 * Based on RIOT/examples/gcoap
 */
int coap_init(sock_udp_ep_t *remote, coap_stats_t *stats)
{
    ipv6_addr_t addr;

    remote->family = AF_INET6;
    coap_stats = stats;

    printf("CoAP target: %s port %d\n", COAP_TARGET, COAP_PORT);

    /* parse for interface */
    char *iface = ipv6_addr_split_iface(COAP_TARGET);

    if (!iface) {
        if (gnrc_netif_numof() == 1) {
            remote->netif = (uint16_t)gnrc_netif_iter(NULL)->pid; /* single addr*/
        }
        else {
            remote->netif = SOCK_ADDR_ANY_NETIF;
        }
    }
    else {
        int pid = atoi(iface);
        if (gnrc_netif_get_by_pid(pid) == NULL) {
            puts("init: interface not valid");
            return 1;
        }
        remote->netif = pid;
    }

    /* parse destination address */
    if (ipv6_addr_from_str(&addr, COAP_TARGET) == NULL) {
        puts("init: unable to parse destination address");
        return 2;
    }
    if ((remote->netif == SOCK_ADDR_ANY_NETIF) &&
        ipv6_addr_is_link_local(&addr)) {
        puts("init: must specify interface for link local target");
        return 3;
    }

    /* set address and port */
    memcpy(&remote->addr.ipv6[0], &addr.u8[0], sizeof(addr.u8));
    remote->port = COAP_PORT;

    return 0;
}

size_t coap_send(uint8_t *buf, size_t len, sock_udp_ep_t *remote)
{
    return gcoap_req_send(buf, len, remote, coap_resp_handler, NULL);
}

/*
 * Response callback.
 * Based on RIOT/examples/gcoap
 */
void coap_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu,
                       const sock_udp_ep_t *remote)
{
    (void)remote;       /* not interested in the source currently */

    if (memo->state == GCOAP_MEMO_TIMEOUT) {
        printf("gcoap: timeout for msg ID %02u\n", coap_get_id(pdu));
        coap_stats->timeouts++;
        return;
    }
    else if (memo->state == GCOAP_MEMO_ERR) {
        printf("gcoap: error in response\n");
        coap_stats->errors++;
        return;
    }

    bool success = coap_get_code_class(pdu) == COAP_CLASS_SUCCESS;
    char *class_str = success ? "Success" : "Error";

    if (success) {
        coap_stats->successes++;
    } else {
        coap_stats->errors++;
    }

    printf("gcoap: response %s, code %1u.%02u", class_str,
           coap_get_code_class(pdu), coap_get_code_detail(pdu));
    if (pdu->payload_len) {
        unsigned content_type = coap_get_content_type(pdu);
        if (content_type == COAP_FORMAT_TEXT
            || content_type == COAP_FORMAT_LINK
            || coap_get_code_class(pdu) == COAP_CLASS_CLIENT_FAILURE
            || coap_get_code_class(pdu) == COAP_CLASS_SERVER_FAILURE) {
            /* Expecting diagnostic payload in failure cases */
            printf(", %u bytes\n%.*s\n", pdu->payload_len, pdu->payload_len,
                   (char *)pdu->payload);
        }
        else {
            printf(", %u bytes\n", pdu->payload_len);
            od_hex_dump(pdu->payload, pdu->payload_len, OD_WIDTH_DEFAULT);
        }
    }
    else {
        printf(", empty payload\n");
    }
}

/* must be sorted by path (ASCII order) */
const coap_resource_t coap_resources[] = {
    COAP_WELL_KNOWN_CORE_DEFAULT_HANDLER,
};

const unsigned coap_resources_numof = sizeof(coap_resources) /
                                      sizeof(coap_resources[0]);
