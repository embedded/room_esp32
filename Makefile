APPLICATION = saul_senml
BOARD      ?= esp32-wroom-32
RIOTBASE   ?= $(CURDIR)/../..
QUIET      ?= 1
DEVELHELP  ?= 0

# Include local settings
include Makefile.local

# Local modules
USEPKG += nanocbor

# SAUL
USEMODULE += saul_default

# Sensors
USEMODULE += hdc1000
USEMODULE += ccs811_full
USEMODULE += bh1750fvi
USEMODULE += bme280_i2c
USEMODULE += senml_saul

CFLAGS += -DBMX280_PARAM_I2C_DEV=I2C_DEV\(0\)
CFLAGS += -DBMX280_PARAM_I2C_ADDR=0x76
CFLAGS += -DCONFIG_HDC1000_I2C_ADDRESS=\(0x40\)
CFLAGS += -DCONFIG_HDC1000_CONVERSION_TIME=1000000ul
CFLAGS += -DCCS811_PARAM_MODE=CCS811_MODE_10S

# Board-specific overrides
ifeq ($(BOARD),esp32-wroom-32)
CFLAGS    += -DCCS811_PARAM_INT_PIN=GPIO18
CFLAGS    += -DCCS811_PARAM_WAKE_PIN=GPIO19
CFLAGS    += -DCCS811_PARAM_RESET_PIN=GPIO5
CFLAGS    += '-DTHREAD_STACKSIZE_MAIN=(THREAD_STACKSIZE_DEFAULT + 4096)'
USEMODULE += esp_wifi
endif

# Networking modules
USEMODULE += netdev_default
USEMODULE += auto_init_gnrc_netif
USEMODULE += gnrc_icmpv6_error
USEMODULE += gnrc_ipv6_default
USEMODULE += gnrc_ipv6
USEMODULE += gnrc_udp
USEMODULE += gnrc_icmpv6_echo

# CoAP
USEMODULE += gcoap
USEMODULE += od

# Misc
USEMODULE += printf_float

# Required peripherals
FEATURES_REQUIRED += periph_timer
FEATURES_REQUIRED += periph_i2c
FEATURES_REQUIRED += periph_spi
FEATURES_REQUIRED += periph_pm

include $(RIOTBASE)/Makefile.include
